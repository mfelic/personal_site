## Personal Website

A simple static site built with gatsby to give visitors info about me, showcase my projects, and provide a way people can contact me.

## Notes

When using styled components and error is thrown when we pass boolean true / false, issue described
in link below. Workaround was to pass 1/0 as props instead of true/false.
