import React from 'react';
import Title from '../Title';
import styles from '../../css/about.module.css';
import { useStaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';
import AniLink from 'gatsby-plugin-transition-link/AniLink';

const getAbout = graphql`
  query aboutImage {
    aboutImage: file(relativePath: { eq: "aboutTeaser.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 600) {
          ...GatsbyImageSharpFluid_tracedSVG
        }
      }
    }
  }
`;

export default function About() {
  const { aboutImage } = useStaticQuery(getAbout);

  return (
    <section className={styles.about}>
      <Title title="about" subtitle="me" />
      <div className={styles.aboutCenter}>
        <article className={styles.aboutImg}>
          <div className={styles.imgContainer}>
            <Img
              fluid={aboutImage.childImageSharp.fluid}
              alt="picture of michael"
            />
          </div>
        </article>
        <article className={styles.aboutInfo}>
          <h4>Problem Solver</h4>
          <p>
            I love learning and the thrill of conquering complex problems. I've
            spent the last {new Date().getFullYear() - 2014} years using my
            skills helping clients brings their ideas to life. In that time I've
            built a variety of websites, mobile applications, and startup
            companies.
          </p>

          <AniLink fade to="/about">
            <button type="button" className="btn-primary">
              Read More
            </button>
          </AniLink>
        </article>
      </div>
    </section>
  );
}
