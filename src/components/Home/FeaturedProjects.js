import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import AniLink from 'gatsby-plugin-transition-link/AniLink';
import Title from '../Title';
import Project from '../Work/Project';
import styles from '../../css/projectList.module.css';

const getProjects = graphql`
  query {
    featuredProjects: allContentfulProject(filter: { featured: { eq: true } }) {
      edges {
        node {
          name
          primaryTag
          slug
          images {
            fluid {
              ...GatsbyContentfulFluid_tracedSVG
            }
          }
          contentful_id
        }
      }
    }
  }
`;

const FeaturedProjects = () => {
  const response = useStaticQuery(getProjects);
  const projects = response.featuredProjects.edges;
  return (
    <section className={styles.projects}>
      <Title title="featured" subtitle="projects" />
      <div className={styles.center}>
        {projects.map(({ node }) => (
          <Project key={node.contentful_id} project={node} />
        ))}
      </div>

      <AniLink fade to="/work" className="btn-primary">
        all projects
      </AniLink>
    </section>
  );
};

export default FeaturedProjects;
