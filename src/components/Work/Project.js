import React from 'react';
import Image from 'gatsby-image';
import styles from '../../css/project.module.css';
import AniLink from 'gatsby-plugin-transition-link/AniLink';
import { useStaticQuery, graphql } from 'gatsby';

const getDefaultImage = graphql`
  query {
    file(relativePath: { eq: "starry.jpeg" }) {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`;

const Project = ({ project }) => {
  const data = useStaticQuery(getDefaultImage);
  const defaultImg = data.file.childImageSharp.fluid;

  const { name, primaryTag, images, slug } = project;

  // Loads default image if retrieval from contentful is unsuccessful
  let projectImage = images ? images[0].fluid : defaultImg;

  return (
    <article className={styles.project}>
      <div className={styles.imgContainer}>
        <Image
          fluid={projectImage}
          className={styles.img}
          alt="single project"
        />
        <AniLink fade to={`/work/${slug}`} className={styles.link}>
          details
        </AniLink>
      </div>
      <div className={styles.footer}>
        <h3>{name}</h3>
        <h4>{primaryTag}</h4>
      </div>
    </article>
  );
};

export default Project;
