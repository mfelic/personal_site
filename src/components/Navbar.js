import React, { useState } from 'react';
import styles from '../css/navbar.module.css';
import { FaAlignRight } from 'react-icons/fa';
import links from '../constants/links';
import { HeaderIcons } from '../constants/social-icons';
import logo from '../images/name.svg';
import AniLink from 'gatsby-plugin-transition-link/AniLink';

export default function Navbar() {
  const [isOpen, setVisibility] = useState(false);

  /**
   * Toggles visibility of Navbar on smaller screens.
   */
  const toggleNav = () => {
    setVisibility(isOpen => !isOpen);
  };

  /**
   * Return styles for nav depending on its current
   * visibility.
   *
   * @returns {String} - appropriate nav styles
   */
  const getNavCSS = () => {
    const navStyles = isOpen
      ? `${styles.navLinks} ${styles.showNav}`
      : `${styles.navLinks}`;

    return navStyles;
  };

  return (
    <nav className={styles.navbar}>
      <div className={styles.navCenter}>
        <div className={styles.navHeader}>
          <AniLink fade to="/">
            <img src={logo} alt="michael feliciano logo" />
          </AniLink>
          <button type="button" className={styles.logoBtn} onClick={toggleNav}>
            <FaAlignRight className={styles.logoIcon} />
          </button>
        </div>

        <div style={{ display: 'flex', alignItems: 'center' }}>
          <ul className={getNavCSS()}>
            {links.map((item, idx) => {
              return (
                <li key={idx}>
                  <AniLink fade to={item.path}>
                    {item.text}
                  </AniLink>
                </li>
              );
            })}
          </ul>
          <div className={styles.navSocialLinks}>
            {HeaderIcons.map((item, idx) => {
              return (
                <a
                  key={idx}
                  href={item.url}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {item.icon}
                </a>
              );
            })}
          </div>
        </div>
      </div>
    </nav>
  );
}
