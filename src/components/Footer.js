import React from 'react';
import styles from '../css/footer.module.css';
import links from '../constants/links';
import { FooterIcons } from '../constants/social-icons';
import AniLink from 'gatsby-plugin-transition-link/AniLink';

export default function Footer() {
  return (
    <footer className={styles.footer}>
      <div className={styles.links}>
        {links.map((item, index) => {
          return (
            <AniLink fade to={item.path} key={index}>
              {item.text}
            </AniLink>
          );
        })}
      </div>
      <div className={styles.icons}>
        {FooterIcons.map((item, idx) => {
          return (
            <a
              key={idx}
              href={item.url}
              target="_blank"
              rel="noopener noreferrer"
            >
              {item.icon}
            </a>
          );
        })}
      </div>
      <div className={styles.copyright}>
        {' '}
        copyright &copy; {new Date().getFullYear()} Michael Feliciano
      </div>
    </footer>
  );
}
