import React from 'react';
import styled from 'styled-components';
import BackgroundImage from 'gatsby-background-image';

const StyledHero = ({ img, className, children, home, position }) => {
  return (
    <BackgroundImage
      className={className}
      fluid={img}
      home={home}
      position={position}
    >
      {children}
    </BackgroundImage>
  );
};

export const Hero = styled(StyledHero)`
  min-height: ${props => (props.home ? 'calc(100vh - 62px)' : '50vh')};
  background: ${props =>
    props.home
      ? 'linear-gradient(rgb(57, 147, 221, 0.55), rgba(0, 0, 0, 0.7))'
      : 'none'};
  opacity: 1 !important;
  background-position: ${props => props.position};
  background-size: cover;
  background-repeat: no-repeat;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const LostHero = ({ img, children }) => {
  return <LostHeroWrapper img={img} children={children} />;
};

const LostHeroWrapper = styled(Hero)`
  min-height: calc(100vh - 62px);
  background: linear-gradient(rgb(0, 0, 0, 0.2), rgba(0, 0, 0, 0.5));
  background-position: center;
  background-size: cover;
`;

export const ProjectHero = ({ img, children }) => {
  return <ProjectHeroWrapper img={img} children={children} />;
};

const ProjectHeroWrapper = styled(StyledHero)`
  min-height: 50vh;
  background: linear-gradient(rgb(0, 0, 0, 0.1), rgba(0, 0, 0, 0.1));
  opacity: 1 !important;
  background-position: center;
  background-size: cover;
`;

StyledHero.defaultProps = {
  home: 0,
};
