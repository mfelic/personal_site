import React from 'react';
import Title from '../Title';
import styles from '../../css/aboutPage.module.css';
import { useStaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';
import AniLink from 'gatsby-plugin-transition-link/AniLink';

const getAbout = graphql`
  query aboutPageImage {
    aboutPageImage: file(relativePath: { eq: "me.jpg" }) {
      childImageSharp {
        fluid(maxHeight: 500) {
          ...GatsbyImageSharpFluid_tracedSVG
        }
      }
    }
  }
`;

const About = () => {
  const { aboutPageImage } = useStaticQuery(getAbout);
  return (
    <section className={styles.about}>
      <Title title="about" subtitle="me" />
      <div className={styles.aboutCenter}>
        <article className={styles.aboutImg}>
          <div className={styles.imgContainer}>
            <Img
              fluid={aboutPageImage.childImageSharp.fluid}
              alt="picture of michael"
            />
          </div>
        </article>

        <article className={styles.aboutInfo}>
          <h2>Builder of Things</h2>
          <p>
            I'm a fullstack software developer currently living and working in
            Columbia, South Carolina. My current stack of choice is HTML, CSS,
            and React.js on the front-end, along with Node.js, AWS services, and
            NoSQL databases on the back-end. I'm a big fan of Gatsby.js for
            building static sites like this one you are viewing now. When I'm
            not coding you will find me reading books, taking online courses, or
            coming up with startup ideas. I love meeting new people and talking
            about tech and business.
          </p>
          <AniLink fade to="/contact">
            <button type="button" className="btn-primary">
              Get In Touch
            </button>
          </AniLink>
        </article>
      </div>
    </section>
  );
};

export default About;
