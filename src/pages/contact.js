import React from 'react';
import Layout from '../components/Layout';
import SEO from '../components/seo';
import { graphql } from 'gatsby';
import { Hero } from '../components/StyledHero';
import Contact from '../components/Contact/Contact';

export default function ContactPage({ data }) {
  return (
    <Layout>
      <SEO
        title="Contact"
        keywords={[`michael`, `feliciano`, 'software', `developer`, `contact`]}
      />
      {/* Passing styled component 1 or 0 instead of true and false
      https://github.com/styled-components/styled-components/issues/1198 */}
      <Hero img={data.contactImage.childImageSharp.fluid} position={'left'} />
      <Contact />
    </Layout>
  );
}

export const query = graphql`
  query {
    contactImage: file(relativePath: { eq: "contact.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 4160, quality: 90) {
          ...GatsbyImageSharpFluid_tracedSVG
        }
      }
    }
  }
`;
