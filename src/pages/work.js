import React from 'react';
import Layout from '../components/Layout';
import SEO from '../components/seo';
import { graphql } from 'gatsby';
import { Hero } from '../components/StyledHero';
import Projects from '../components/Work/Projects';

export default function WorkPage({ data }) {
  return (
    <Layout>
      <SEO
        title="Work"
        keywords={[
          `michael`,
          `feliciano`,
          'software',
          `developer`,
          `portfolio`,
          `projects`,
        ]}
      />
      <Hero img={data.workImage.childImageSharp.fluid} position={'center'}>
        {''}
      </Hero>
      <Projects />
    </Layout>
  );
}

export const query = graphql`
  query {
    workImage: file(relativePath: { eq: "work.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 4160, quality: 90) {
          ...GatsbyImageSharpFluid_tracedSVG
        }
      }
    }
  }
`;
