import React from 'react';
import Layout from '../components/Layout';
import SEO from '../components/seo';
import { graphql } from 'gatsby';
import { Hero } from '../components/StyledHero';
import About from '../components/About/About';

export default function AboutPage({ data }) {
  return (
    <Layout>
      <SEO
        title="About"
        keywords={[`michael`, `feliciano`, 'software', `developer`]}
      />
      <Hero img={data.aboutImage.childImageSharp.fluid} position={'center'}>
        {''}
      </Hero>
      <About />
    </Layout>
  );
}

export const query = graphql`
  query {
    aboutImage: file(relativePath: { eq: "aboutHero.jpeg" }) {
      childImageSharp {
        fluid(maxWidth: 4160, quality: 90) {
          ...GatsbyImageSharpFluid_tracedSVG
        }
      }
    }
  }
`;
