import React from 'react';
import { Link } from 'gatsby';
import Banner from '../components/Banner';
import Layout from '../components/Layout';
import { graphql } from 'gatsby';
import { LostHero } from '../components/StyledHero';
import SEO from '../components/seo';

export default function NotFoundPage({ data }) {
  return (
    <Layout>
      <SEO title="Not Found" />
      <LostHero img={data.lostImage.childImageSharp.fluid}>
        <Banner title="Looks like you're lost friend">
          <Link to="/" className="btn-white">
            Back to home page
          </Link>
        </Banner>
      </LostHero>
    </Layout>
  );
}

export const query = graphql`
  query {
    lostImage: file(relativePath: { eq: "map-compass.jpeg" }) {
      childImageSharp {
        fluid(maxWidth: 4160, quality: 90) {
          ...GatsbyImageSharpFluid_tracedSVG
        }
      }
    }
  }
`;
