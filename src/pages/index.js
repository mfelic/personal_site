import React from 'react';
import Layout from '../components/Layout';
import Banner from '../components/Banner';
import AniLink from 'gatsby-plugin-transition-link/AniLink';
import SEO from '../components/seo';
import About from '../components/Home/About';
import Services from '../components/Home/Services';
import { Hero } from '../components/StyledHero';
import { graphql } from 'gatsby';
import FeaturedProjects from '../components/Home/FeaturedProjects';

export default ({ data }) => (
  <Layout>
    <SEO
      title="Home"
      keywords={[`michael`, `feliciano`, 'software', `developer`]}
    />
    <Hero
      home={1}
      img={data.homeHeroImage.childImageSharp.fluid}
      position={'center'}
    >
      <Banner title="Software Developer">
        <AniLink fade to="/work" className="btn-white">
          See My Work
        </AniLink>
      </Banner>
    </Hero>
    <About />
    <Services />
    <FeaturedProjects />
  </Layout>
);

export const query = graphql`
  query {
    homeHeroImage: file(relativePath: { eq: "homeHero.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 4160, quality: 90) {
          ...GatsbyImageSharpFluid_tracedSVG
        }
      }
    }
  }
`;
