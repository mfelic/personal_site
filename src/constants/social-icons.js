import React from 'react';
import { FaLinkedin, FaGitlab } from 'react-icons/fa';

import upwork from '../images/upwork.svg';
import upworkBlue from '../images/upworkBlue.svg';

export const HeaderIcons = [
  {
    icon: <img src={upworkBlue} width={22} height={22} alt="upwork logo" />,
    url: 'https://www.upwork.com/fl/michaelfeliciano5',
  },
  {
    icon: <FaLinkedin />,
    url: 'https://www.linkedin.com/in/michael-feliciano-5bb18740/',
  },
  {
    icon: <FaGitlab />,
    url: 'https://gitlab.com/mfelic',
  },
];

export const FooterIcons = [
  {
    icon: <img src={upwork} width={22} height={22} alt="upwork logo" />,
    url: 'https://www.upwork.com/fl/michaelfeliciano5',
  },
  {
    icon: <FaLinkedin />,
    url: 'https://www.linkedin.com/in/michael-feliciano-5bb18740/',
  },
  {
    icon: <FaGitlab />,
    url: 'https://gitlab.com/mfelic',
  },
];
