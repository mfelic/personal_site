import React from 'react';
import { FaMap, FaPencilRuler, FaHardHat } from 'react-icons/fa';

export default [
  {
    icon: <FaMap />,
    title: 'Roadmap',
    text:
      'We work together to flesh out your vision, and develop a plan covering the steps required to go from idea to working product.',
  },
  {
    icon: <FaPencilRuler />,
    title: 'Prototyping',
    text:
      'We turn your idea into a usable prototype that looks and feels real. A prototype is great for getting feedback from potential customers, or pitching investors.',
  },
  {
    icon: <FaHardHat />,
    title: 'Development',
    text:
      "My team and I will collaborate with you to design and build a website or app that you'll be thrilled to use yourself or show the world.",
  },
];
