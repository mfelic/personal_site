import React from 'react';
import { graphql } from 'gatsby';
import Layout from '../components/Layout';
import { ProjectHero } from '../components/StyledHero';
import styles from '../css/template.module.css';
import Img from 'gatsby-image';
import AniLink from 'gatsby-plugin-transition-link/AniLink';
import SEO from '../components/seo';

const projectTemplate = ({ data }) => {
  const {
    name,
    allTags,
    projectLink,
    description: { description },
    images,
  } = data.project;

  const [mainImage, ...projectImages] = images;

  // project Images must be the same height to
  // get desired aesthetic look.
  // More specifically a change to something like 2560:1380
  // in the resize -> scale for the header image will do the trick

  return (
    <Layout>
      <SEO title={name} />
      <ProjectHero img={mainImage.fluid} />
      <section className={styles.template} />
      <div className={styles.center}>
        <div className={styles.images}>
          {projectImages.map((item, idx) => {
            return (
              <Img
                key={idx}
                fluid={item.fluid}
                alt="single project"
                className={styles.image}
              />
            );
          })}
        </div>

        <div className={styles.projectInfo}>
          <h1>
            <span>{name}</span>
          </h1>
          <h2> Technologies Used:</h2>
          {allTags.map((item, idx) => {
            return (
              <h4 key={idx} className={styles.technologies}>
                {item}
              </h4>
            );
          })}
          {projectLink ? (
            <>
              <h2>Project Link: </h2>
              <a
                href={projectLink}
                target="_blank"
                rel="noopener noreferrer"
                className={styles.projectLink}
              >
                www.thecodesmith.co
              </a>
            </>
          ) : null}
          <h2 className={styles.descTitle}>Description: </h2>
          <p className={styles.desc}>{description}</p>
        </div>
        <div />

        <AniLink fade to="/work" className="btn-primary">
          Back to Projects
        </AniLink>
      </div>
    </Layout>
  );
};

export const query = graphql`
  query($slug: String!) {
    project: contentfulProject(slug: { eq: $slug }) {
      name
      allTags
      projectLink
      description {
        description
      }
      images {
        fluid {
          ...GatsbyContentfulFluid_tracedSVG
        }
      }
    }
  }
`;

export default projectTemplate;
